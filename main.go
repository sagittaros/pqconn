package pqconn

import (
	"database/sql"
	"log"

	_ "github.com/lib/pq" // using Postgres library
)

// EstablishConnection to Postgres
func EstablishConnection(connectionString string) *sql.DB {
	var (
		err  error
		conn *sql.DB
	)

	conn, err = sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	if err = conn.Ping(); err != nil {
		log.Fatal(err)
	}
	return conn
}

func CloseDBConnection(conn *sql.DB) {
	if err := conn.Close(); err != nil {
		log.Fatal(err)
	}
}
